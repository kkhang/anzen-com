import { Col, DatePicker, Form, Input, Row, Space } from 'antd'
import dayjs from 'dayjs';
import React from 'react'

const CreateBillingList = () => {
    const dateFormat = 'DD/MM/YYYY';

    return (
        <div>
            <Col span={24}>
                <h1 style={{ textAlign: "center", color: "#e61e1e", fontSize: "1.5em", fontWeight: "500" }}>BẢNG KÊ GIAO NHẬN VẬN CHUYỂN</h1>
            </Col>
            <Form
                labelCol={{
                    span: 24,
                }}
                wrapperCol={{
                    span: 24,
                }}>
                <Row>
                    <Col span={8} >
                        <Form.Item
                            className='ant-form-billing'
                            colon={true}
                            label="Mã bảng kê"
                        // name="Code"
                        >
                            <Input />
                        </Form.Item>
                    </Col>
                    <Col span={8} >
                        <Form.Item
                            className='ant-form-billing'
                            colon={true}
                            label="Mã bảng kê"
                        // name="Code"
                        >
                            <DatePicker
                                format={dateFormat}
                                defaultValue={dayjs()}
                                style={{
                                    width: '100%',
                                }} />
                        </Form.Item>
                    </Col>
                    <Col span={8} >
                        <Form.Item
                            className='ant-form-billing'
                            colon={true}
                            label="Hợp đồng số"
                        // name="Code"
                        >
                            <Input />
                        </Form.Item>
                    </Col>
                    <Col span={8} >
                        <Form.Item
                            className='ant-form-billing'
                            colon={true}
                            label="Tên công ty"
                        // name="Code"
                        >
                            <Input />
                        </Form.Item>
                    </Col>
                    <Col span={8} >
                        <Form.Item
                            className='ant-form-billing'
                            colon={true}
                            label="Số điện thoại"
                        // name="Code"
                        >
                            <Input />
                        </Form.Item>
                    </Col>
                    <Col span={8} >
                        <Form.Item
                            className='ant-form-billing'
                            colon={true}
                            label="MST"
                        // name="Code"
                        >
                            <Input />
                        </Form.Item>
                    </Col>
                    <Col span={8} >
                        <Form.Item
                            className='ant-form-billing'
                            colon={true}
                            label="Người lái xe"
                        // name="Code"
                        >
                            <Input />
                        </Form.Item>
                    </Col>
                    <Col span={8} >
                        <Form.Item
                            className='ant-form-billing'
                            colon={true}
                            label="Biến số xe"
                        // name="Code"
                        >
                            <Input />
                        </Form.Item>
                    </Col>
                    <Col span={8} ></Col>
                    <Col span={8} >
                        <Form.Item
                            className='ant-form-billing'
                            colon={true}
                            label="Địa chỉ"
                        // name="Code"
                        >
                            <Input />
                        </Form.Item>
                    </Col>
                    <Col span={8} >
                        <Form.Item
                            className='ant-form-billing'
                            colon={true}
                            label="CMND"
                        // name="Code"
                        >
                            <Input />
                        </Form.Item>
                    </Col>
                    <Col span={8} >
                        <Form.Item
                            className='ant-form-billing'
                            colon={true}
                            label="Đã tạm ứng"
                        // name="Code"
                        >
                            <Input />
                        </Form.Item>
                    </Col>
                    <Col span={8} >
                        <Form.Item
                            className='ant-form-billing'
                            colon={true}
                            label="Giấy phép lái xe"
                        // name="Code"
                        >
                            <Input />
                        </Form.Item>
                    </Col>
                    <Col span={8} >
                        <Form.Item
                            className='ant-form-billing'
                            colon={true}
                            label="Điện thoại lái xe"
                        // name="Code"
                        >
                            <Input />
                        </Form.Item>
                    </Col>
                    <Col span={8} >
                        <Form.Item
                            className='ant-form-billing'
                            colon={true}
                            label="Tổng cước cho xe"
                        // name="Code"
                        >
                            <Input />
                        </Form.Item>
                    </Col>
                </Row>
            </Form>
            <Row>
                <Col span={15}></Col>
                <Col span={9}>
                    <Space>
                        <button className='btn-order'>Danh sách hàng cần đi</button>
                        <button className='btn-order'>Xoá dữ liệu để tạo mới</button>
                        <button className='btn-order'>GỬI</button>
                    </Space>
                </Col>
            </Row>
        </div>
    )
}

export default CreateBillingList
