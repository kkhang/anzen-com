import { Button, Dropdown, Layout, Menu, Space, theme } from 'antd';
import React, { useEffect, useState } from 'react'
import logoAnzen from '../../assets/logo.png'

import {
    MenuFoldOutlined,
    MenuUnfoldOutlined,
    UserOutlined,
    HomeFilled,
    // Icon
} from '@ant-design/icons';
import { Link, useNavigate } from 'react-router-dom';
import Order from '../../pages/Order';
import List from '../../pages/List';
import Customer from '../../pages/Customer';
import Driver from '../../pages/Driver';
import CreateAccount from '../../pages/CreateAccount';
import './styles.css'
import ListIcon from '../../assets/ListIcon';
import CustomerIcon from '../../assets/CustomerIcon';
import DriverIcon from '../../assets/DriverIcon';
import AccountIcon from '../../assets/AccountIcon';
import { getUserProfileService } from '../../service/loginService';

const MenuAnzen = () => {
    const [user, setUser] = useState("")
    // const dispatch = useDispatch()
    const navigate = useNavigate()
    const { Header, Sider, Content } = Layout;
    useEffect(() => {
        getUserProfile();
    }, [])
    const getUserProfile = async () => {
        const res = await getUserProfileService();
        if (res.status !== 200) {
            navigate('/');
        }
        setUser(res?.data?.result)
    }
    const [collapsed, setCollapsed] = useState(false);
    const {
        token: { colorBgContainer },
    } = theme.useToken();
    const [selectedKey, setSelectedKey] = useState('1');

    const handleMenuClick = (key) => {
        setSelectedKey(key);
    };

    const renderContent = () => {
        switch (selectedKey) {
            case '1':
                return <Order user = {user} />;
            case '2':
                return <List />;
            case '3':
                return <Customer />;
            case '4':
                return <Driver />;
            case '5':
                return <CreateAccount />;
            default:
                return null;
        }
    };
    const handleMenu = (e) => {
        if (e.key === 'logout') {
            // Xử lý logic đăng xuất tại đây
            localStorage.removeItem('accessToken');
            navigate('/login')
        }
    };
    const menu = (
        <Menu onClick={handleMenu}>
            <Menu.Item key="logout">
                Đăng xuất
            </Menu.Item>
        </Menu>
    );
    return (
        <Layout>
            <Sider trigger={null} collapsible collapsed={collapsed}>
                <div className="demo-logo-vertical" >
                    {collapsed ?
                        <>
                            <img src={logoAnzen} alt='' />
                        </>
                        : (
                            <>
                                <img src={logoAnzen} alt='' />
                                <h1>ANZEN</h1>
                            </>
                        )}
                </div>
                <Menu
                    theme="dark"
                    mode="inline"
                    defaultSelectedKeys={['1']}
                    onClick={(e) => handleMenuClick(e.key)}
                    items={[
                        {
                            key: '1',
                            icon: <HomeFilled />,
                            label: <Link to='/order'>Khu vực đơn hàng</Link>,
                        },
                        {
                            key: '2',
                            icon: <ListIcon />,
                            label: <Link to='/list'>Bảng kê</Link>,
                        },
                        {
                            key: '3',
                            icon: <CustomerIcon />,
                            label: <Link to='customer'>Khách hàng</Link>,
                        },
                        {
                            key: '4',
                            icon: <DriverIcon />,
                            label: <Link to='/driver'>Tài xế</Link>,
                        },
                        {
                            key: '5',
                            icon: <AccountIcon />,
                            label: <Link to='/account'>Tạo tài khoản</Link>,
                        },
                    ]}
                />
            </Sider>
            <Layout>
                <Header
                    style={{
                        padding: 0,
                        background: colorBgContainer,
                    }}
                >
                    <Button
                        type="text"
                        icon={collapsed ? <MenuUnfoldOutlined /> : <MenuFoldOutlined />}
                        onClick={() => setCollapsed(!collapsed)}
                        style={{
                            fontSize: '16px',
                            width: 64,
                            height: 64,
                        }}
                    />

                    <Dropdown overlay={menu}>
                        <Space>
                            <UserOutlined style={{ color: '#ccc' }} />
                            <span className='user'>{user?.userName}</span>
                        </Space>

                    </Dropdown>


                </Header>
                <Content
                    style={{
                        // margin: '24px 16px',
                        padding: 24,
                        minHeight: 280,
                        background: colorBgContainer,
                    }}
                >
                    {renderContent()}
                </Content>
            </Layout>
        </Layout>
    );
}

export default MenuAnzen
