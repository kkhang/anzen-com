import { Checkbox, Col, DatePicker, Form, Input, Row, Space, Table, Tag } from 'antd'
import dayjs from 'dayjs';
import React, { useEffect, useState } from 'react'
import { FundOutlined } from '@ant-design/icons';
import { useDispatch, useSelector } from 'react-redux';
import { getAllBillingAsync, selectBilling } from '../../pages/List/billingSlice';
import { createSlice } from '@reduxjs/toolkit';

const BillingList = () => {
    const { RangePicker } = DatePicker;
    const dateFormat = 'DD/MM/YYYY';
    const [pages, setPages] = useState({ filter: '', PageIndex: 1, PageSize: 10 })
    const dispatch = useDispatch()
    const bill = useSelector(selectBilling)
    useEffect(() => {
        dispatch(getAllBillingAsync(pages))
    }, [])
    console.log(bill)
    const columns = [
        {
            title: 'STT',   //1
            dataIndex: 'additionalAmount',
            key: 'additionalAmount',
            with: "10%",
            render: (text, record, index) => {
                const stt = (pages.PageIndex - 1) * pages.PageSize + index + 1
                return stt
            }
        },
        {
            title: 'Số mã',   //1
            dataIndex: 'code',
            key: 'code',
            render: (text) => {
                if (text) {
                    return <Tag style={{ backgroundColor: "rgb(244, 35, 35)", color: "white", borderRadius: "15px" }}>{text}</Tag>;
                }
                return <Tag style={{ backgroundColor: "rgb(244, 35, 35)", width: "20px", height: "3px", borderRadius: "0" }}></Tag>
            },
        },
        {
            title: 'Ngày tạo',   //1
            dataIndex: 'createdDate',
            key: 'createdDate',
            render: (date) => dayjs(date).format('DD/MM/YYYY'),
        },
        {
            title: 'Số điện thoại đối tác',   //1
            dataIndex: 'partnerPhone',
            key: 'partnerPhone',
            render: (date) => dayjs(date).format('DD/MM/YYYY'),
        },
        {
            title: 'Tài xế',   //1
            dataIndex: 'partner',
            key: 'partner',
        },
        {
            title: 'SDT Tài xế',   //1
            dataIndex: 'driverPhone',
            key: 'driverPhone',
        },
        {
            title: 'Tổng số tiền',   //1
            dataIndex: 'totalFreight',
            key: 'totalFreight',
        },
        {
            title: 'Thao tác',
            dataIndex: 'actions',
            key: 'actions',
            render: (_, record) => (
                <Space>
                    <button >Sửa</button>
                    <FundOutlined style={{ color: "rgb(255, 189, 47)", fontSize: "24px" }} />
                </Space>
            )
        },

    ]
    const handleTableChange = (pages) => {
        // setIsLoading(true)
        const params = {
            PageIndex: pages.current,
            PageSize: 10
        }
        setPages(params)
        dispatch(getAllBillingAsync(pages))
    }
    return (
        <div>
            <Form
                labelCol={{
                    span: 24,
                }}
                wrapperCol={{
                    span: 24,
                }}>
                <Row>
                    <Col span={6} >
                        <Form.Item
                            className='ant-form-billing'
                            colon={true}
                            label="Mã bảng kê"
                        // name="Code"
                        >
                            <Input />
                        </Form.Item>
                    </Col>
                    <Col span={6}
                        className='ant-form-billing'
                    >
                        <Form.Item
                            colon={true}
                            label="Biển số xe"
                        // name="Code"
                        >
                            <Input />
                        </Form.Item>
                    </Col>
                    <Col span={6}
                        className='ant-form-billing'
                    >
                        <Form.Item
                            colon={true}
                            label="SDT tài xế"
                        // name="Code"
                        >
                            <Input />
                        </Form.Item>
                    </Col>
                    <Col span={6}
                        className='ant-form-billing'
                    >
                        <Form.Item
                            colon={true}
                            label="Từ ngày - đến ngày"
                        // name="Code"
                        >
                            <RangePicker
                                format={dateFormat}
                                defaultValue={[dayjs("02/01/2022", dateFormat), dayjs()]}
                            />
                        </Form.Item>
                    </Col>
                </Row>
            </Form>
            <Col span={24} style={{ padding: "0" }}>
                <Space>
                    <button className='btn-order' >
                        Clear
                    </button>
                    <button className='btn-order'>
                        Tìm kiếm
                    </button>
                    <button className='btn-order'>
                        Export Excel
                    </button>
                    <button className='btn-order'>
                        Tạo mới đơn
                    </button>
                    <Checkbox
                    // onChange={advanceResult}
                    >
                        Tìm kiếm nâng cao
                    </Checkbox>

                </Space>
                <Col span={24} className='mt-5'>
                    <Table
                        // style={{ width: "100%" }}
                        dataSource={bill?.Billing?.items}
                        loading={bill?.isLoading}
                        columns={columns}
                        pagination={{ size: "small", total: bill?.deliveryOrder?.total, showTotal: (total, range) => `${range[0]}-${range[1]} of ${total} items` }}
                        onChange={(page) => handleTableChange(page)}
                    /></Col>
            </Col>



        </div>
    )
}

export default BillingList
