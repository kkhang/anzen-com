import { BrowserRouter, Route, Routes } from 'react-router-dom';
import './App.css';
import Login from './pages/Login';
import DefaultLayout from './component/DefaultLayout';

function App() {
  return (
      <BrowserRouter>
        <Routes>
          <Route path='/' element={<Login /> } />
          <Route path='/*' element={<DefaultLayout /> } />
        </Routes>
      </BrowserRouter>
  );
}

export default App;
