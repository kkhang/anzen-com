import React, { useState } from 'react'
import './styles.css'
import { Button, Form, Input, Spin, notification } from 'antd'
import { useTranslation } from 'react-i18next'

import logoAnzen from '../../assets/logo.png'
import { useNavigate } from 'react-router-dom'
import { loginService } from '../../service/loginService'
const Login = () => {
  const [isLoading, setIsLoading] = useState(false)
  const { i18n, t } = useTranslation()
  const trans = (lang) => {
    i18n.changeLanguage(lang)
  }
  const [Username, setUsername] = useState('')
  const [Password, setPassword] = useState('')
  // const [isLoading, setIsLoading] = useState('')

  const navigate = useNavigate()
  const onFinish = async (values) => {
    setIsLoading(true)
    const response = await loginService(values)

    const { status, data } = response
    if (status === 200) {
      navigate('/order') //chuyển hướng đến trang home
      // notification.success({ //thông báo đăng nhập thành công
      //     message: 'Đăng nhập thành công!',
      //     duration: 1,
      //     description: 'Sai tên đăng nhập hoặc mật khẩu!',
      // });
      const token = data.access_token
      localStorage.setItem('accessToken', token) //lưu trữ token để tự động đăng nhập
    } else {
      notification.error({
        message: 'Đăng nhập sai!',
        duration: 3,
        description: 'Sai tên đăng nhập hoặc mật khẩu!',
      })
    }
    setIsLoading(false)
  }

  return (
    <div className='login'>
      <Spin spinning={isLoading} size='large' tip='Loading...'>
        <div className='login-main'>
          <img src={logoAnzen} alt='' />
          <p>
            Welcome to
            <span className='spanlogo'> ANZEN VẬN TẢI</span>
          </p>
          <Form
            className='login-form'
            name='basic'
            style={{
              maxWidth: 600,
            }}
            initialValues={{
              remember: true,
            }}
            onFinish={onFinish}
            autoComplete='off'
          >
            <Form.Item
              name='username'
              rules={[
                {
                  required: true,
                  message: 'Please input your username!',
                },
              ]}
            >
              <Input className='input-login' />
            </Form.Item>

            <Form.Item
              name='password'
              rules={[
                {
                  required: true,
                  message: 'Please input your password!',
                },
              ]}
            >
              <Input.Password className='input-login' />
            </Form.Item>

            <Form.Item
              wrapperCol={{
                offset: 0,
                span: 16,
              }}
            >
              <Button type='primary' htmlType='submit'>
                Login
              </Button>
            </Form.Item>
          </Form>
        </div>
      </Spin>
    </div>
  )
}

export default Login
