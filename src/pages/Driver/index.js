import {
  Button,
  Checkbox,
  Col,
  DatePicker,
  Form,
  Input,
  Modal,
  Popconfirm,
  Row,
  Space,
  Table,
} from 'antd'
import { EditOutlined, FundOutlined, DeleteOutlined } from '@ant-design/icons'
import { useEffect, useState } from 'react'
import React from 'react'
import { useDispatch, useSelector } from 'react-redux'
import {
  createDriverAsync,
  deleteDriverAsync,
  getAllDriverAsync,
  selectDriver,
  updateDriverAsync,
} from './driverSlice'
import dayjs from 'dayjs'
const { RangePicker } = DatePicker
//date format
const dateFormat = 'DD/MM/YYYY'
const Driver = () => {
  const dispatch = useDispatch()
  const [pages, setPages] = useState({ PageIndex: 1, PageSize: 10 })
  const driverState = useSelector(selectDriver)
  //form create driver
  const [formSearchDriver] = Form.useForm()
  const [formDriver] = Form.useForm()
  const [advance, setAdvance] = useState()
  //modal create driver
  const [isOpenModalDriver, setIsOpenModalDriver] = useState(false)
  //console.log(driverState);
  useEffect(() => {
    dispatch(getAllDriverAsync(pages))
  }, [])

  //form create driver
  const onFinishDriver = async (values) => {
    //console.log(values);
    if (values.id) {
      await dispatch(updateDriverAsync(values))
    } else {
      await dispatch(createDriverAsync(values))
    }

    formDriver.resetFields()
    dispatch(getAllDriverAsync(pages))
    setIsOpenModalDriver(false)
  }
  const onFinishSearchDriver = async (values) => {
    //console.log(dayjs(values?.dateSearch?.[0]).format("YYYY-MM-DD"));
    const params = {
      ...values,
      CreatedDateFrom: dayjs(values?.CreatedDateFrom?.[0]).format('YYYY-MM-DD'),
      CreatedDateTo: dayjs(values?.CreatedDateTo?.[1]).format('YYYY-MM-DD'),
    }
    console.log(values)
    delete params.CreatedDateFrom
    await dispatch(getAllDriverAsync({ ...params, ...pages }))
  }
  const showModalDriver = () => {
    setIsOpenModalDriver(true)
  }

  const handleOkDriver = () => {
    setIsOpenModalDriver(false)
  }

  const handleCancelDriver = () => {
    setIsOpenModalDriver(false)
  }
  //handle update
  const handleEditDriver = async (values) => {
    console.log(values)
    formDriver.resetFields()
    showModalDriver()
    formDriver.setFieldsValue({ ...values })
  }
  //handle delete
  const handleDelete = async (values) => {
    console.log(values)
    await dispatch(deleteDriverAsync(values.id))
    dispatch(getAllDriverAsync(pages))
  }
  //on change button tìm kiếm nâng cao
  const onChange = (e) => {
    setAdvance(e.target.checked)
  }
  //name columns
  const columns = [
    {
      title: 'STT', //1
      dataIndex: 'additionalAmount',
      key: 'additionalAmount',
      with: '10%',
      render: (text, record, index) => {
        // console.log('num123',pages)
        const stt = (pages.PageIndex - 1) * pages.PageSize + index + 1
        return stt
      },
    },
    {
      title: 'Tên',
      dataIndex: 'name',
      key: 'name',
    },
    {
      title: 'Số điện thoại',
      dataIndex: 'phone',
      key: 'phone',
    },
    {
      title: 'CMND/CCD',
      dataIndex: 'identity',
      key: 'identity',
    },
    {
      title: 'Địa chỉ',
      dataIndex: 'phone',
      key: 'phone',
    },
    {
      title: 'Bằng lái xe',
      dataIndex: 'drivingLicense',
      key: 'drivingLicense',
    },
    {
      title: 'Biển số xe',
      dataIndex: 'licensePlate',
      key: 'licensePlate',
    },
    {
      title: 'Công ty',
      dataIndex: 'company',
      key: 'company',
    },
    {
      title: 'SĐT công ty',
      dataIndex: 'companyPhone',
      key: 'companyPhone',
    },
    {
      title: 'Chi tiết',
      dataIndex: 'description',
      key: 'description',
    },
    {
      title: 'Hoạt động',
      key: 'activity',
      with: '10%',
      render: (_, record) => (
        <Space size='middle'>
          <EditOutlined
            style={{ color: 'green', fontSize: '24px' }}
            onClick={() => handleEditDriver(record)}
          />
          <Popconfirm
            title='Bạn có đồng ý xóa không?'
            onConfirm={() => handleDelete(record)}
          >
            <DeleteOutlined
              style={{ color: 'rgb(255, 77, 79)', fontSize: '24px' }}
            />
          </Popconfirm>

          <FundOutlined
            style={{ color: 'rgb(255, 189, 47)', fontSize: '24px' }}
          />
        </Space>
      ),
    },
  ]
  const handleTableChange = (pages) => {
    const params = {
      PageIndex: pages.current,
      PageSize: 10,
    }
    setPages(params)
    dispatch(getAllDriverAsync(params))
  }
  return (
    <div className='container'>
      <Row>
        <Form
          labelCol={{
            span: 24,
          }}
          wrapperCol={{
            span: 24,
          }}
          form={formSearchDriver}
          name='formSearchDriver'
          onFinish={onFinishSearchDriver}
        >
          <Row>
            <Col span={6}>
              <Form.Item colon={false} label='Biển số xe' name='licensePlate'>
                <Input className='input-item' />
              </Form.Item>
            </Col>
            <Col span={6}>
              <Form.Item colon={false} label='Tên' name='Name'>
                <Input className='input-item' />
              </Form.Item>
            </Col>
            <Col span={6}>
              <Form.Item colon={false} label='Số Điện Thoại' name='phone'>
                <Input className='input-item' />
              </Form.Item>
            </Col>
            <Col span={6}>
              <Form.Item label='Từ ngày - Đến ngày' name='dateSearch'>
                <RangePicker
                  format={dateFormat}
                  defaultValue={[dayjs('01/01/2022', dateFormat), dayjs()]}
                />
              </Form.Item>
            </Col>
          </Row>
          {advance && (
            <Row>
              <Col span={6}>
                <Form.Item colon={false} label='Tên công ty' name='Company'>
                  <Input className='input-item' />
                </Form.Item>
              </Col>
              <Col span={6}>
                <Form.Item
                  colon={false}
                  label='SĐT công ty'
                  name='CompanyPhone'
                >
                  <Input className='input-item' />
                </Form.Item>
              </Col>
              <Col span={6}>
                <Form.Item
                  colon={false}
                  label='Bằng lái xe'
                  name='DrivingLicense'
                >
                  <Input className='input-item' />
                </Form.Item>
              </Col>
              <Col span={6}>
                <Form.Item label='CMND/CCCD' name='Identity'>
                  <Input className='input-item' />
                </Form.Item>
              </Col>
            </Row>
          )}

          <Col span={18}>
            <Space>
              <button className='btn-order'>Clear</button>
              <button className='btn-order' htmlType='submit'>
                Tìm kiếm
              </button>
              <button className='btn-order'>Export Excel</button>
              <button className='btn-order' onClick={showModalDriver}>
                Tạo mới Tài Xế
              </button>
              <Checkbox className='checkbox-input' onChange={onChange}>
                Tìm kiếm nâng cao
              </Checkbox>
            </Space>
          </Col>
        </Form>
      </Row>

      <Row>
        <Modal
          title='Tạo mới Tài Xế'
          open={isOpenModalDriver}
          onOk={handleOkDriver}
          onCancel={handleCancelDriver}
          width={1000}
          footer={[
            <Button key='cancel' onClick={handleCancelDriver}>
              Cancel
            </Button>,
            <Button key='ok' type='primary' form='formDriver' htmlType='submit'>
              OK
            </Button>,
          ]}
        >
          <Form
            name='formDriver'
            form={formDriver}
            labelCol={{
              span: 8,
            }}
            wrapperCol={{
              span: 16,
            }}
            style={{
              maxWidth: 600,
            }}
            onFinish={onFinishDriver}
            autoComplete='off'
          >
            <Form.Item label='ID' name='id' hidden>
              <Input />
            </Form.Item>
            <Form.Item
              label='Tên tài xế'
              name='name'
              rules={[
                {
                  required: true,
                  message: 'Please input name!',
                },
              ]}
            >
              <Input placeholder='Nhập Tên' />
            </Form.Item>
            <Form.Item
              label='Số điện thoại'
              name='phone'
              rules={[
                {
                  required: true,
                  message: 'Please input phone number!',
                },
              ]}
            >
              <Input placeholder='Nhập Số điện thoại' />
            </Form.Item>

            <Form.Item
              label='Địa chỉ'
              name='address'
              rules={[
                {
                  required: true,
                  message: 'Please input address!',
                },
              ]}
            >
              <Input placeholder='Nhập địa chỉ' />
            </Form.Item>

            <Form.Item
              label='CMND/CCCD'
              name='identity'
              rules={[
                {
                  required: true,
                  message: 'Please input identity!',
                },
              ]}
            >
              <Input placeholder='Nhập CMND/CCCD của tài xế' />
            </Form.Item>

            <Form.Item
              label='Bằng lái xe'
              name='drivingLicense'
              rules={[
                {
                  required: true,
                  message: 'Please input phone drivingLicense!',
                },
              ]}
            >
              <Input placeholder='Nhập bằng lái xe của tài xế' />
            </Form.Item>
            <Form.Item
              label='Biển số xe'
              name='licensePlate'
              rules={[
                {
                  required: true,
                  message: 'Please input license plate!',
                },
              ]}
            >
              <Input placeholder='Nhập biển số xe' />
            </Form.Item>
            <Form.Item
              label='Tên công ty'
              name='company'
              rules={[
                {
                  required: false,
                  message: 'Please input company!',
                },
              ]}
            >
              <Input placeholder='Nhập tên công ty' />
            </Form.Item>
            <Form.Item
              label='Số điện thoại công ty'
              name='companyPhone'
              rules={[
                {
                  required: false,
                  message: 'Please input companyPhone!',
                },
              ]}
            >
              <Input placeholder='Nhập số điện thoại công ty' />
            </Form.Item>
            <Form.Item
              label='Thông tin chi tiết'
              name='description'
              rules={[
                {
                  required: false,
                  message: 'Please input description!',
                },
              ]}
            >
              <Input placeholder='Nhập thông tin chi tiết' />
            </Form.Item>
          </Form>
        </Modal>
      </Row>
      <Row className='mt-5'>
        <Table
          style={{ width: '100%' }}
          dataSource={driverState?.driverList?.items}
          loading={driverState.isLoading}
          columns={columns}
          pagination={{
            size: 'small',
            total: driverState?.driverList?.total,
            showTotal: (total, range) =>
              `${range[0]}-${range[1]} of ${total} items`,
          }}
          onChange={(page) => handleTableChange(page)}
        />
      </Row>
    </div>
  )
}

export default Driver
