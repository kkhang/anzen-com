import { createAsyncThunk, createSlice } from "@reduxjs/toolkit";
import {
  createDriverService,
  deleteDriverService,
  getAllDriverService,
  updateDriverService,
} from "./../../service/driverService";

const initialState = {
  isLoading: false,
  driverList: {},
};
export const getAllDriverAsync = createAsyncThunk(
  "getAllDriver",
  async (pages) => {
    const response = await getAllDriverService(pages);
    // console.log(response);
    return response.data.result;
  }
);
export const createDriverAsync = createAsyncThunk(
  "createDriver",
  async (pages) => {
    const response = await createDriverService(pages);
    // console.log(response);
    return response.data.result;
  }
);
export const updateDriverAsync = createAsyncThunk(
  "updateDriver",
  async (pages) => {
    const response = await updateDriverService(pages);
    // console.log(response);
    return response.data.result;
  }
);
export const deleteDriverAsync = createAsyncThunk(
  "deleteDriver",
  async (pages) => {
    const response = await deleteDriverService(pages);
    // console.log(response);
    return response.data.result;
  }
);
export const silceDriver = createSlice({
  name: "driverSlice",
  initialState,
  reducers: {},
  extraReducers: (builder) => {
    builder
      .addCase(getAllDriverAsync.pending, (state) => {
        state.isLoading = true;
      })
      .addCase(getAllDriverAsync.fulfilled, (state, action) => {
        if (action.payload) {
          state.isLoading = false;
          state.driverList = action.payload;
        }
      })
      .addCase(createDriverAsync.pending, (state) => {
        state.isLoading = true;
      })
      .addCase(createDriverAsync.fulfilled, (state, action) => {
        if (action.payload) {
          state.isLoading = false;
          state.driverList = action.payload;
        }
      })
      .addCase(updateDriverAsync.pending, (state) => {
        state.isLoading = true;
      })
      .addCase(updateDriverAsync.fulfilled, (state, action) => {
        if (action.payload) {
          state.isLoading = false;
          state.driverList = action.payload;
        }
      })
      .addCase(deleteDriverAsync.pending, (state) => {
        state.isLoading = true;
      })
      .addCase(deleteDriverAsync.fulfilled, (state, action) => {
        if (action.payload) {
          state.isLoading = false;
          state.driverList = action.payload;
        }
      });
  },
});
export const selectDriver = (state) => state.driverSlice;
export default silceDriver.reducer;
