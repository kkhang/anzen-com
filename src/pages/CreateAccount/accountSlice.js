import { createAsyncThunk, createSlice } from "@reduxjs/toolkit";
import {
  createAccountService,
  getAllAccountService,
  getAllRoleService,
  updateIsActiveService,
  updateRoleService,
} from "../../service/accountService";

const initialState = {
  isLoading: false,
  accountList: {},
  roleList: [],
  active: "",
  role: "",
};
export const getAllAccountAsync = createAsyncThunk(
  "getAllAccount",
  async (pages) => {
    const response = await getAllAccountService(pages);
    // console.log(response);
    return response.data.result;
  }
);
export const getAllRoleAsync = createAsyncThunk("getAllRole", async (pages) => {
  const response = await getAllRoleService(pages);
  // console.log(response);
  return response.data;
});
export const createAccountAsync = createAsyncThunk(
  "createAccount",
  async (params) => {
    const response = await createAccountService(params);
    // console.log(response);
    return response.data;
  }
);
export const updateIsActiveAsync = createAsyncThunk(
  "updateIsActive",
  async (data) => {
    const response = await updateIsActiveService(data);
    console.log("data service", response);
    return response.data;
  }
);
export const updateRoleAsync = createAsyncThunk("updateRole", async (data) => {
  const response = await updateRoleService(data);
  console.log("data service", response);
  return response.data;
});
export const silceAccount = createSlice({
  name: "accountSlice",
  initialState,
  reducers: {},
  extraReducers: (builder) => {
    builder
      .addCase(getAllAccountAsync.pending, (state) => {
        state.isLoading = true;
      })
      .addCase(getAllAccountAsync.fulfilled, (state, action) => {
        if (action.payload) {
          state.isLoading = false;
          state.accountList = action.payload;
        }
      })
      .addCase(getAllRoleAsync.pending, (state) => {
        state.isLoading = true;
      })
      .addCase(getAllRoleAsync.fulfilled, (state, action) => {
        if (action.payload) {
          state.isLoading = false;
          state.roleList = action.payload;
        }
      })
      .addCase(createAccountAsync.pending, (state) => {
        state.isLoading = true;
      })
      .addCase(createAccountAsync.fulfilled, (state, action) => {
        if (action.payload) {
          state.isLoading = false;
          state.accountList = action.payload;
        }
      })
      .addCase(updateIsActiveAsync.pending, (state) => {
        state.isLoading = true;
      })
      .addCase(updateIsActiveAsync.fulfilled, (state, action) => {
        if (action.payload) {
          state.isLoading = false;
          state.active = action.payload;
        }
      })
      .addCase(updateRoleAsync.pending, (state) => {
        state.isLoading = true;
      })
      .addCase(updateRoleAsync.fulfilled, (state, action) => {
        if (action.payload) {
          state.isLoading = false;
          state.role = action.payload;
        }
      });
  },
});
export const selectAccount = (state) => state.accountSlice;
export default silceAccount.reducer;
