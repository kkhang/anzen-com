import React, { useEffect, useState } from "react";
import { useDispatch, useSelector } from "react-redux";
import {
  createAccountAsync,
  getAllAccountAsync,
  getAllRoleAsync,
  selectAccount,
  updateIsActiveAsync,
  updateRoleAsync,
} from "./accountSlice";
import {
  Row,
  Space,
  Switch,
  Table,
  Button,
  Modal,
  Form,
  Input,
  Select,
  AutoComplete,
} from "antd";
// import "./styles.css";

const CreateAccount = () => {
  useEffect(() => {
    dispatch(getAllAccountAsync(pages)); //get all account
    dispatch(getAllRoleAsync(pages)); //get all role
  }, []);
  const dispatch = useDispatch();
  const accountState = useSelector(selectAccount);
  const { roleList } = accountState;
  const [pages, setPages] = useState({ PageIndex: 1, PageSize: 10 });
  const [isModalOpen, setIsModalOpen] = useState(false);
  const [isOpenModalRole, setIsOpenModalRole] = useState(false);
  const showModal = () => {
    setIsModalOpen(true);
  };
  const showModalRole = () => {
    setIsOpenModalRole(true);
  };
  const handleOk = () => {
    setIsModalOpen(false);
  };
  const handleOkRole = () => {
    setIsOpenModalRole(false);
  };
  const handleCancel = () => {
    setIsModalOpen(false);
  };
  const handleCancelRole = () => {
    setIsOpenModalRole(false);
  };
  const [form] = Form.useForm();
  const [formRole] = Form.useForm();
  //create account
  const onFinish = async (values) => {
    await dispatch(createAccountAsync(values));
    form.resetFields();
    dispatch(getAllAccountAsync(pages));
    setIsModalOpen(false);
  };
  //thay đổi trạng thái active
  const onChange = (checked) => { };
  const handleStatusAccount = async (record) => {
    const data = { id: record.id, isActive: !record.isActive };
    await dispatch(updateIsActiveAsync(data));
    dispatch(getAllAccountAsync(pages));
  };
  //thay đổi quyền
  const submitRole = async (values) => {
    await dispatch(updateRoleAsync(values));
    form.resetFields();
    dispatch(getAllAccountAsync(pages));
    setIsOpenModalRole(false);
  };
  const columns = [
    {
      title: "STT", //1
      dataIndex: "additionalAmount",
      key: "additionalAmount",
      with: "10%",
      render: (text, record, index) => {
        // console.log('num123',pages)
        const stt = (pages.PageIndex - 1) * pages.PageSize + index + 1;
        return stt;
      },
    },
    {
      title: "Tên đầy đủ", //2
      dataIndex: "fullName",
      key: "fullName",
      with: "10%",
    },
    {
      title: "Tên tài khoản", //3
      dataIndex: "userName",
      key: "userName",
      with: "10%",
    },
    {
      title: "Địa chỉ", //4
      dataIndex: "address",
      key: "address",
      with: "10%",
    },
    {
      title: "Email", //5
      dataIndex: "email",
      key: "email",
      with: "10%",
    },
    {
      title: "Số điện thoại", //6
      dataIndex: "phoneNumber",
      key: "phoneNumber",
      with: "10%",
    },
    {
      title: "Quyền", //7
      dataIndex: "roleName",
      key: "roleName",
      with: "10%",
    },
    {
      title: "Hoạt động", //8
      key: "activity",
      with: "10%",
      render: (text, record) => (
        <Space size="middle">
          <Switch
            checked={record.isActive}
            onChange={onChange}
            onClick={() => handleStatusAccount(record)}
          //style={{ background: "#ffbd2f" }}
          />
          <button
            danger
            className="btn-edit-role"
            onClick={() => handleEditRole(record)}
            style={{
              background: "white",
              padding: "4px 15px",
              border: "1px solid #ff4d4f",
              color: "#ff4d4f",
            }}
          >
            Sửa quyền
          </button>
        </Space>
      ),
    },
  ];

  const handleEditRole = (record) => {
    console.log(record);
    showModalRole();
    formRole.resetFields();
    formRole.setFieldsValue({ roleName: record.roleName, id: record.id });
  };
  const handleTableChange = (pages) => {
    const params = {
      PageIndex: pages.current,
      PageSize: 10,
    };
    setPages(params);
    dispatch(getAllAccountAsync(params));
  };
  return (
    <div>
      <Modal
        title="THAY ĐỔI QUYỀN"
        open={isOpenModalRole}
        onOk={handleOkRole}
        onCancel={handleCancelRole}
        footer={[
          <Button key="cancel" onClick={handleCancelRole}>
            Cancel
          </Button>,
          <Button key="ok" type="primary" form="formRole" htmlType="submit">
            OK
          </Button>,
        ]}
      >
        <Form
          name="formRole"
          form={formRole}
          labelCol={{
            span: 8,
          }}
          wrapperCol={{
            span: 16,
          }}
          style={{
            maxWidth: 600,
          }}
          onFinish={submitRole}
          autoComplete="off"
        >
          <Form.Item label="ID" name="id" hidden>
            <Input />
          </Form.Item>
          <Form.Item
            label="Quyền"
            name="roleName"
            rules={[
              {
                required: true,
                message: "Please input roleName!",
              },
            ]}
          >
            <Select>
              {roleList?.result?.items.map((item, index) => (
                <Select.Option key={index} value={item.name}>
                  {item.description}
                </Select.Option>
              ))}
            </Select>
          </Form.Item>
        </Form>
      </Modal>
      <Row>
        <Button className="btn-account" type="primary" onClick={showModal}>
          Tạo mới tài khoản
        </Button>

        <Modal
          title="Tạo mới tài khoản khách hàng"
          open={isModalOpen}
          onOk={handleOk}
          onCancel={handleCancel}
          width={1000}
          footer={[
            <Button key="cancel" onClick={handleCancel}>
              Cancel
            </Button>,
            <Button key="ok" type="primary" form="form" htmlType="submit">
              OK
            </Button>,
          ]}
        >
          <Form
            name="form"
            form={form}
            labelCol={{
              span: 8,
            }}
            wrapperCol={{
              span: 16,
            }}
            style={{
              maxWidth: 600,
            }}
            onFinish={onFinish}
            autoComplete="off"
          >
            <Form.Item label="ID" name="id" hidden>
              <Input />
            </Form.Item>
            <Form.Item
              label="Tên"
              name="name"
              rules={[
                {
                  required: true,
                  message: "Please input name!",
                },
              ]}
            >
              <Input />
            </Form.Item>

            <Form.Item
              label="Tên đầy đủ"
              name="fullName"
              rules={[
                {
                  required: true,
                  message: "Please input fullName!",
                },
              ]}
            >
              <Input />
            </Form.Item>

            <Form.Item
              label="Số điện thoại"
              name="phoneNumber"
              rules={[
                {
                  required: true,
                  message: "Please input phone Number!",
                },
              ]}
            >
              <Input />
            </Form.Item>

            <Form.Item
              label="Địa chỉ"
              name="address"
              rules={[
                {
                  required: true,
                  message: "Please input address!",
                },
              ]}
            >
              <Input />
            </Form.Item>

            <Form.Item
              label="Tên đăng nhập"
              name="userName"
              rules={[
                {
                  required: true,
                  message: "Please input userName!",
                },
              ]}
            >
              <Input />
            </Form.Item>

            <Form.Item
              label="Mật khẩu"
              name="password"
              rules={[
                {
                  required: true,
                  message: "Please input password!",
                },
              ]}
            >
              <Input.Password />
            </Form.Item>

            <Form.Item
              name="confirm"
              label="Confirm Password"
              dependencies={["password"]}
              hasFeedback
              rules={[
                {
                  required: true,
                  message: "Please confirm your password!",
                },
                ({ getFieldValue }) => ({
                  validator(_, value) {
                    if (!value || getFieldValue("password") === value) {
                      return Promise.resolve();
                    }
                    return Promise.reject(
                      new Error(
                        "The new password that you entered do not match!"
                      )
                    );
                  },
                }),
              ]}
            >
              <Input.Password />
            </Form.Item>

            <Form.Item
              label="Quyền"
              name="roleName"
              rules={[
                {
                  required: true,
                  message: "Please input roleName!",
                },
              ]}
            >
              <Select placeholder="Chọn quyền">
                {roleList?.result?.items.map((item, index) => (
                  <Select.Option key={index} value={item.name}>
                    {item.description}
                  </Select.Option>
                ))}
              </Select>
            </Form.Item>
          </Form>
        </Modal>
      </Row>
      <Row className="mt-5">
        <Table
          style={{ width: "100%" }}
          dataSource={accountState?.accountList?.items}
          loading={accountState.isLoading}
          columns={columns}
          pagination={{
            size: "small",
            total: accountState?.accountList?.total,
            showTotal: (total, range) =>
              `${range[0]}-${range[1]} of ${total} items`,
          }}
          onChange={(page) => handleTableChange(page)}
        />
      </Row>
    </div>
  );
};

export default CreateAccount;
