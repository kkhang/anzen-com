import { useEffect, useState } from 'react'
import React from 'react'
import { Table, Space, Form, Input, Modal, DatePicker, Col, Row } from 'antd'
import { EditOutlined, DeleteOutlined } from '@ant-design/icons'
import { useDispatch, useSelector } from 'react-redux'
import './index.css'
import {
  getCustomerAsync,
  deleteCustomerAsync,
  selectCustomer,
  editCustomerAsync,
  createCustomerAsync,
} from './customerSlice'
import dayjs from 'dayjs'

const Customer = () => {
  const dateFormat = 'DD/MM/YYYY'
  const customerState = useSelector(selectCustomer)
  const { RangePicker } = DatePicker
  const [form] = Form.useForm()
  const [formSearch] = Form.useForm()
  const [pages, setPages] = useState({ PageIndex: 1, PageSize: 10 })
  const dispatch = useDispatch()
  useEffect(() => {
    dispatch(getCustomerAsync(pages))
  }, [pages])

  const columns = [
    {
      title: 'STT',
      dataIndex: 'id',
      key: 'id',
      render: (text, record, index) => {
        const stt = (pages.PageIndex - 1) * pages.PageSize + index + 1
        return stt
      },
    },
    {
      title: 'Tên',
      dataIndex: 'name',
      key: 'name',
    },
    {
      title: 'Số điện thoại',
      dataIndex: 'phone',
      key: 'phone',
    },
    {
      title: 'Địa chỉ',
      dataIndex: 'address',
      key: 'address',
    },
    {
      title: 'Email',
      dataIndex: '',
      key: '',
    },
    {
      title: 'Chi tiết khách hàng',
      dataIndex: 'description',
      key: 'description',
    },
    {
      title: 'Hoạt động',
      key: 'address',
      render: (_, record) => (
        <Space size='middle'>
          <EditOutlined
            style={{ color: 'green', fontSize: '24px' }}
            onClick={() => editCustomer(record)}
          >
            Edit
          </EditOutlined>
          <DeleteOutlined
            style={{ color: 'rgb(255, 77, 79)', fontSize: '24px' }}
            onClick={() => deleteCustomer(record.id)}
          >
            Delete
          </DeleteOutlined>
        </Space>
      ),
    },
  ]

  const deleteCustomer = async (id) => {
    await dispatch(deleteCustomerAsync(id))
    dispatch(getCustomerAsync(id))
  }

  const editCustomer = (record) => {
    form.resetFields()
    setIsModalOpen(true)
    form.setFieldsValue({
      ...record,
    })
  }
  const [isModalOpen, setIsModalOpen] = useState(false)
  const showModal = () => {
    setIsModalOpen(true)
    form.resetFields()
  }
  const handleCancel = () => {
    setIsModalOpen(false)
  }
  useEffect(() => {
    form.setFieldsValue({})
  }, [])
  const onFinish = async (values) => {
    if (values.id) {
      await dispatch(editCustomerAsync(values))
    } else {
      await dispatch(createCustomerAsync(values))
    }
    dispatch(getCustomerAsync())
    setIsModalOpen(false)
  }
  const handleTableChange = (page) => {
    const params = {
      PageIndex: page.current,
      PageSize: 10,
    }
    setPages(params)
  }
  const handleClear = () => {
    formSearch.resetFields()
  }
  const handleSearch = async (values) => {
    const params = {
      ...values,
      CreatedDateFrom: dayjs(values?.createdDate?.[0]).format('YYYY-MM-DD'),
      CreatedDateTo: dayjs(values?.createdDate?.[1]).format('YYYY-MM-DD'),
    }
    delete params.createdDate
    await dispatch(getCustomerAsync({ ...params, ...pages }))
  }
  return (
    <div className='container'>
      <div className='header'>
        <Row>
          <Form
            labelCol={{
              span: 24,
            }}
            wrapperCol={{
              span: 24,
            }}
            form={formSearch}
            name='formSearch'
            onFinish={handleSearch}
          >
            <Row>
              <Col span={8}>
                <Form.Item colon={false} label='Số điện thoại' name='phone'>
                  <Input className='input-item' />
                </Form.Item>
              </Col>
              <Col span={8}>
                <Form.Item colon={false} label='Tên' name='Name'>
                  <Input className='input-item' />
                </Form.Item>
              </Col>
              <Col span={8}>
                <Form.Item
                  label='Từ ngày - Đến ngày'
                  name='createdDate'
                  initialValue={[dayjs('01/01/2022', dateFormat), dayjs()]}
                >
                  <RangePicker format={dateFormat} />
                </Form.Item>
              </Col>
            </Row>
          </Form>
          <Col span={18}>
            <Space>
              <button className='btn-order' onClick={handleClear}>
                Clear
              </button>
              <button className='btn-order' htmlType='submit' form='formSearch'>
                Tìm kiếm
              </button>
              <button className='btn-order'>Export Excel</button>
              <button className='btn-order' onClick={showModal}>
                Tạo mới khách hàng
              </button>
            </Space>
          </Col>
        </Row>
      </div>
      <div className='table'>
        <Modal
          title='Customer'
          open={isModalOpen}
          footer={[
            <button key='cancel' onClick={handleCancel} className='btn-order'>
              Cancel
            </button>,
            <button
              key='ok'
              htmlType='submit'
              type='primary'
              form='form'
              className='btn-order'
            >
              OK
            </button>,
          ]}
        >
          <Form
            name='form'
            form={form}
            labelCol={{
              span: 8,
            }}
            wrapperCol={{
              span: 16,
            }}
            style={{
              maxWidth: 600,
            }}
            onFinish={onFinish}
            autoComplete='off'
          >
            <Form.Item label='ID' name='id' style={{ display: 'none' }}>
              <Input />
            </Form.Item>
            <Form.Item
              label='Name'
              name='name'
              rules={[
                {
                  required: true,
                  message: 'Please input name!',
                },
              ]}
            >
              <Input />
            </Form.Item>
            <Form.Item
              label='Phone'
              name='phone'
              rules={[
                {
                  required: true,
                  message: 'Please input phone!',
                },
              ]}
            >
              <Input />
            </Form.Item>
            <Form.Item
              label='Email'
              name='email'
              rules={[
                {
                  required: true,
                  message: 'Please input email!',
                },
              ]}
            >
              <Input />
            </Form.Item>
            <Form.Item
              label='Address'
              name='address'
              rules={[
                {
                  required: true,
                  message: 'Please input address!',
                },
              ]}
            >
              <Input />
            </Form.Item>
            <Form.Item
              label='Description'
              name='description'
              rules={[
                {
                  required: true,
                  message: 'Please input description!',
                },
              ]}
            >
              <Input />
            </Form.Item>
            <Form.Item
              wrapperCol={{
                offset: 8,
                span: 16,
              }}
            ></Form.Item>
          </Form>
        </Modal>
        <Table
          dataSource={customerState?.customer?.items}
          loading={customerState.isLoading}
          columns={columns}
          pagination={{
            size: 'small',
            total: customerState?.customer?.total,
            showTotal: (total, range) =>
              `${range[0]}-${range[1]} of ${total} items`,
          }}
          onChange={(page) => handleTableChange(page)}
        />
      </div>
    </div>
  )
}

export default Customer
