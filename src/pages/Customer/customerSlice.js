import { createAsyncThunk, createSlice } from '@reduxjs/toolkit'
import {
  getCustomerService,
  deleteCustomerService,
  editCustomerService,
  createCustomerService,
} from './../../service/customerService'
const initialState = {
  isLoading: false,
  customer: {},
}
export const getCustomerAsync = createAsyncThunk(
  'getCustomer',
  async (pages) => {
    const response = await getCustomerService(pages)
    return response.data.result
  }
)

export const deleteCustomerAsync = createAsyncThunk(
  'deleteCustomer',
  async (id) => {
    const response = await deleteCustomerService(id)
    return response.data
  }
)

export const editCustomerAsync = createAsyncThunk(
  'editCustomer',
  async (params) => {
    const response = await editCustomerService(params)
    return response.data
  }
)

export const createCustomerAsync = createAsyncThunk(
  'createCustomer',
  async (params) => {
    const response = await createCustomerService(params)
    return response.data
  }
)

export const customerSlice = createSlice({
  name: 'customerData',
  initialState,
  reducers: {},
  extraReducers: (builder) => {
    builder
      .addCase(getCustomerAsync.pending, (state) => {
        state.isLoading = true
      })
      .addCase(getCustomerAsync.fulfilled, (state, action) => {
        if (action.payload) {
          state.isLoading = false
          state.customer = action.payload
        }
      })

      .addCase(createCustomerAsync.pending, (state) => {
        state.isLoading = true
      })
      .addCase(createCustomerAsync.fulfilled, (state, action) => {
        state.isLoading = false
        state.customer = action.payload
      })

      .addCase(deleteCustomerAsync.pending, (state) => {
        state.isLoading = true
      })
      .addCase(deleteCustomerAsync.fulfilled, (state, action) => {
        if (action.payload) {
          state.isLoading = false
          state.customer = action.payload
        }
      })

      .addCase(editCustomerAsync.pending, (state) => {
        state.isLoading = true
      })
      .addCase(editCustomerAsync.fulfilled, (state, action) => {
        if (action.payload) {
          state.isLoading = false
          state.customer = action.payload
        }
      })
  },
})

export const selectCustomer = (state) => state.customerData
export default customerSlice.reducer
