import { Tabs } from 'antd';
import React from 'react'
import { AndroidOutlined, AppleOutlined } from '@ant-design/icons';
import './styles.css'
import BillingList from '../../component/BillingList';
import CreateBillingList from '../../component/CreateBillingList';
const List = () => {
  return (
    <div>
      <Tabs
        defaultActiveKey="1"
        items={[AppleOutlined, AndroidOutlined].map((Icon, i) => {
          const id = String(i + 1);
          let label, content
          if (id === "1") {
            label = (
              <span>
                <Icon />
                Danh sách bảng kê
              </span>
            );
            content = <BillingList />
          } else if (id === "2") {
            label = (
              <span>
                <Icon />
                Tạo mới bảng kê
              </span>
            );
            content = <CreateBillingList />;
          }
          return {
            label,
            key: id,
            children: content,
          };
        })}
      />
    </div>
  )
}

export default List
