import { createAsyncThunk, createSlice } from "@reduxjs/toolkit";
import { getAllBillingService } from "../../service/billingService";
const initialState = {
    isLoading: false,
    Billing: []
}
export const getAllBillingAsync = createAsyncThunk('getAllBilling', async (pages) => {
    const response = await getAllBillingService(pages);
    // console.log(response.data.result)
    return response.data.result
})
export const sliceBilling = createSlice({
    name: 'billingSlice',
    initialState,
    reducers: {},
    extraReducers: builder => {
        builder
            .addCase(getAllBillingAsync.pending, state => {
                state.isLoading = true;
            })
            .addCase(getAllBillingAsync.fulfilled, (state, action) => {
                // console.log('action',action.payload)
                if (action.payload) {
                    state.isLoading = false;
                    state.Billing = action.payload
                }
            })

    }
})
export const selectBilling = state => state.billingSlice;
export default sliceBilling.reducer;