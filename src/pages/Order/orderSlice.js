import { createAsyncThunk, createSlice } from "@reduxjs/toolkit";
import { createOrderService, deleteOrderService, editOrderService, getAllOrderService, getDetailOfCodeService, getOneOrderService, getSalseStaffService } from "../../service/orderService";
const initialState = {
    isLoading: false,
    deliveryOrder: [],
    order: [],
    staff: [],
}
export const getAllOrderAsync = createAsyncThunk('getAllOrder', async (pages) => {
    const response = await getAllOrderService(pages);
    // console.log(response.data.result)
    return response.data.result
})
export const getOneOrderAsync = createAsyncThunk('getOneOrder', async (id) => {
    const response = await getOneOrderService(id);
    // console.log(response.data.result)
    return response.data.result
})
export const getOneOrderOfCodeAsync = createAsyncThunk('getOneOrderOfCode', async (id) => {
    const response = await getDetailOfCodeService(id);
    // console.log(response.data.result)
    return response.data.result
})

export const getSaleStaffAsync = createAsyncThunk('getSaleStaff', async () => {
    const response = await getSalseStaffService();
    // console.log(response.data.result)
    return response.data.result
})
export const createOrderAsync = createAsyncThunk('createOrder', async (data) => {
    const response = await createOrderService(data);
    // console.log(response.data.result)
    return response.data.result
})
export const editOrderAsync = createAsyncThunk('editOrder', async (data) => {
    const response = await editOrderService(data);
    // console.log(response.data.result)
    return response.data.result
})
export const deleteOrderAsync = createAsyncThunk('deleteOrder', async (id) => {
    const response = await deleteOrderService(id);
    return response.data.result
})
export const sliceOrder = createSlice({
    name: 'orderSlice',
    initialState,
    reducers: {},
    extraReducers: builder => {
        builder
            .addCase(getAllOrderAsync.pending, state => {
                state.isLoading = true;
            })
            .addCase(getAllOrderAsync.fulfilled, (state, action) => {
                // console.log('action',action.payload)
                if (action.payload) {
                    state.isLoading = false;
                    state.deliveryOrder = action.payload
                }
            })
            .addCase(getSaleStaffAsync.pending, state => {
                state.isLoading = true;
            })
            .addCase(getSaleStaffAsync.fulfilled, (state, action) => {
                if (action.payload) {
                    state.isLoading = false;
                    state.staff = action.payload
                }
            })
            .addCase(createOrderAsync.pending, state => {
                state.isLoading = true;
            })
            .addCase(createOrderAsync.fulfilled, (state, action) => {
                if (action.payload) {
                    state.isLoading = false;
                    state.order = action.payload
                }
            })
            .addCase(getOneOrderAsync.pending, state => {
                state.isLoading = true;
            })
            .addCase(getOneOrderAsync.fulfilled, (state, action) => {
                if (action.payload) {
                    state.isLoading = false;
                    state.order = action.payload
                }
            })
            .addCase(getOneOrderOfCodeAsync.pending, state => {
                state.isLoading = true;
            })
            .addCase(getOneOrderOfCodeAsync.fulfilled, (state, action) => {
                if (action.payload) {
                    state.isLoading = false;
                    state.order = action.payload
                }
            })
            .addCase(editOrderAsync.pending, state => {
                state.isLoading = true;
            })
            .addCase(editOrderAsync.fulfilled, (state, action) => {
                if (action.payload) {
                    state.isLoading = false;
                    state.order = action.payload
                }
            })
            .addCase(deleteOrderAsync.pending, state => {
                state.isLoading = true;
            })
            .addCase(deleteOrderAsync.fulfilled, (state, action) => {
                if (action.payload) {
                    state.isLoading = false;
                    state.order = action.payload
                }
            })
            
    }
})
export const selectOrder = state => state.orderSlice;
export default sliceOrder.reducer;