/* eslint-disable react-hooks/exhaustive-deps */
import React, { useEffect, useState } from 'react'
import { useDispatch, useSelector } from 'react-redux'
import {
  Checkbox,
  Col,
  Form,
  Input,
  Radio,
  Row,
  Select,
  Space,
  Table,
  Tag,
  Modal,
  DatePicker,
  InputNumber,
  Tooltip,
  Card,
  notification,
} from 'antd'
import './styles.css'
import {
  createOrderAsync,
  deleteOrderAsync,
  editOrderAsync,
  getAllOrderAsync,
  getOneOrderAsync,
  getOneOrderOfCodeAsync,
  getSaleStaffAsync,
  selectOrder,
} from './orderSlice'
import { EditOutlined, DeleteOutlined, FundOutlined } from '@ant-design/icons'
import { v4 as uuidv4 } from 'uuid'
import { PROVINCE } from '../../utils/province'
import dayjs from 'dayjs'
const Order = ({ user }) => {
  useEffect(() => {
    dispatch(getAllOrderAsync(pages))
    dispatch(getSaleStaffAsync())
  }, [])
  const dispatch = useDispatch()
  const order = useSelector(selectOrder)
  const [pages, setPages] = useState({ PageIndex: 1, PageSize: 10 })
  const [saleStaff, setSaleStaff] = useState('')
  const { RangePicker } = DatePicker
  const { Option } = Select
  const [deliveryOrderDetails, setDeliveryOrderDetails] = useState([])

  const columns = [
    {
      title: 'STT', //1
      dataIndex: 'additionalAmount',
      key: 'additionalAmount',
      with: '10%',
      render: (text, record, index) => {
        const stt = (pages.PageIndex - 1) * pages.PageSize + index + 1
        return stt
      },
    },
    {
      title: 'Ngày tạo', //2
      dataIndex: 'orderDate',
      key: 'orderDate',
      with: '10%',
      render: (date) => dayjs(date).format('DD/MM/YYYY'),
    },
    {
      title: 'Số mã', //3
      dataIndex: 'code',
      key: 'code',
      with: '10%',
      render: (text, record) => {
        if (text) {
          return (
            <Tag
              style={{
                backgroundColor: 'rgb(244, 35, 35)',
                color: 'white',
                borderRadius: '15px',
              }}
              onClick={() => showModalOfBill(record.id)}
            >
              {text}
            </Tag>
          )
        }
        return (
          <Tag
            style={{
              backgroundColor: 'rgb(244, 35, 35)',
              width: '20px',
              height: '3px',
              borderRadius: '0',
            }}
          ></Tag>
        )
      },
    },
    {
      title: 'NVKD', //4
      dataIndex: 'saleStaff',
      key: 'saleStaff',
      with: '10%',
    },
    {
      title: 'Tên hàng', //5
      dataIndex: 'name',
      key: 'name',
      with: '10%',
    },
    {
      title: 'Điểm nhận hàng', //6
      dataIndex: 'fromAddress',
      key: 'fromAddress',
      with: '10%',
    },
    {
      title: 'SDT người gửi', //7
      dataIndex: 'shipperPhone', //none
      key: 'shipperPhone',
      with: '10%',
    },
    {
      title: 'Điểm giao hàng', //8
      dataIndex: 'toAddress',
      key: 'toAddress',
      with: '30%',
    },
    {
      title: 'SDT người nhận',
      dataIndex: 'consigneePhone', //none
      key: 'consigneePhone',
    },
    {
      title: 'Giá cước',
      dataIndex: 'totalAmount', //none
      key: 'totalAmount',
      render: (text) => <span>{text.toLocaleString()}</span>,
    },
    {
      title: 'Trạng thái',
      dataIndex: 'status',
      key: 'status',
      render: (text) => {
        if (text === 'New') {
          return (
            <Tooltip title='Đơn mới'>
              <Tag
                style={{
                  backgroundColor: '#0a7cff',
                  width: '30px',
                  height: '15px',
                  borderRadius: '15px',
                }}
              ></Tag>
            </Tooltip>
          )
        } else if (text === 'Inventory') {
          return (
            <Tooltip title='Đơn hàng tồn kho'>
              <Tag
                style={{
                  backgroundColor: '#ff4d4f',
                  width: '30px',
                  height: '15px',
                  borderRadius: '15px',
                }}
              ></Tag>
            </Tooltip>
          )
        } else if (text === 'Gone') {
          return (
            <Tooltip title='Đơn hàng đã đi'>
              <Tag
                style={{
                  backgroundColor: '#04aa6d',
                  width: '30px',
                  height: '15px',
                  borderRadius: '15px',
                }}
              ></Tag>
            </Tooltip>
          )
        } else {
          return (
            <Tooltip title='Chuyển tải'>
              <Tag
                style={{
                  backgroundColor: '#ffbd2f',
                  width: '30px',
                  height: '15px',
                  borderRadius: '15px',
                }}
              ></Tag>
            </Tooltip>
          )
        }
        // console.log('trạng thái', text)
      },
    },
    {
      title: 'HTTT',
      dataIndex: 'paymentType',
      key: 'paymentType',
    },
    {
      title: 'Hoàn tất',
      dataIndex: 'isDone',
      key: 'isDone',
      render: (text) => {
        if (text === false) {
          return (
            <Tooltip title='Chưa hoàn tất'>
              <Tag
                style={{
                  backgroundColor: '#ff4d4f',
                  width: '30px',
                  height: '15px',
                  borderRadius: '15px',
                }}
              ></Tag>
            </Tooltip>
          )
        } else {
          return (
            <Tooltip title='Đã hoàn tất'>
              <Tag
                style={{
                  backgroundColor: '#04aa6d',
                  width: '30px',
                  height: '15px',
                  borderRadius: '15px',
                }}
              ></Tag>
            </Tooltip>
          )
        }
      },
    },
    {
      title: 'Thao tác',
      dataIndex: 'actions',
      key: 'actions',
      render: (_, record) => (
        <Space>
          <EditOutlined
            style={{ color: 'green', fontSize: '24px' }}
            onClick={() => handleEdit(record.id)}
          />
          <DeleteOutlined
            style={{ color: 'rgb(255, 77, 79)', fontSize: '24px' }}
            onClick={() => handleDeleteOrder(record.id)}
          />
          <FundOutlined
            style={{ color: 'rgb(255, 189, 47)', fontSize: '24px' }}
          />
        </Space>
      ),
    },
  ]
  const columModal = [
    {
      title: 'Tên hàng',
      dataIndex: 'name',
      key: 'name',
    },
    {
      title: 'ĐVT',
      dataIndex: 'additionalAmount',
      key: 'additionalAmount',
    },
    {
      title: 'Số lượng',
      dataIndex: 'quantity',
      key: 'quantity',
    },
    {
      title: 'Khối lượng',
      dataIndex: 'mass',
      key: 'mass',
    },
    {
      title: 'Trọng lượng',
      dataIndex: 'weight',
      key: 'weight',
    },
    {
      title: 'Ghi chú',
      dataIndex: 'note',
      key: 'note',
    },
    {
      title: 'Thao tác',
      dataIndex: 'action',
      key: 'action',
      render: (_, record) => (
        <Space>
          <button
            className='btnEdit-formProduct'
            onClick={() => EditProduct(record)}
          >
            Sửa
          </button>
          <button
            className='btnDelete-formProduct'
            onClick={() => DeleteProduct(record.id)}
          >
            Xoá
          </button>
        </Space>
      ),
    },
  ]
  const columnBill1 = [
    {
      title: 'Tên hàng', //1
      dataIndex: 'name',
      key: 'name',
    },
    {
      title: 'ĐVT', //1
      dataIndex: 'unit',
      key: 'unit',
    },
    {
      title: 'Số lượng', //1
      dataIndex: 'quantity',
      key: 'quantity',
    },
    {
      title: 'Khối lượng', //1
      dataIndex: 'mass',
      key: 'mass',
    },
    {
      title: 'Trọng lượng', //1
      dataIndex: 'weight',
      key: 'weight',
    },
    {
      title: 'Ghi chú', //1
      dataIndex: 'note',
      key: 'note',
    },
  ]
  const [isExpanded, setIsExpanded] = useState(false)
  const isColumns = isExpanded
    ? columns
    : [
        columns[0],
        columns[1],
        columns[2],
        columns[3],
        columns[4],
        columns[5],
        columns[7],
        columns[10],
        columns[11],
        columns[12],
        columns[13],
      ]
  const handleRadioChange = (e) => {
    setIsExpanded(e.target.value === 'expand')
  }
  const handleTableChange = (pages) => {
    // setIsLoading(true)
    const params = {
      PageIndex: pages.current,
      PageSize: 10,
    }
    setPages(params)
    dispatch(getAllOrderAsync(params))
  }
  // //modal
  const [isModalOpen, setIsModalOpen] = useState(false)
  const [isGenCode, setIsGenCode] = useState(false)

  const showModal = () => {
    form.resetFields()
    setIsModalOpen(true)
    setSaleStaff(user?.userName)
    console.log(saleStaff)
  }
  const handleOk = () => {
    setIsModalOpen(false)
  }
  const handleCancel = () => {
    setIsModalOpen(false)
    form.resetFields()
    formProduct.resetFields()
    setDeliveryOrderDetails([])
  }
  const [form] = Form.useForm()
  const onFinish = async (values) => {
    if (!values.id) {
      if (deliveryOrderDetails?.length === 0) {
        notification.error({
          //thông báo đăng nhập thành công
          message: 'Thông báo',
          duration: 1,
          description: 'Vui lòng thêm sản phẩm vận chuyển!',
        })
      } else {
        values.orderDate = form.getFieldValue('orderDate').format('YYYY-MM-DD')
        values.isGenCode = isGenCode
        values.saleStaff = saleStaff
        const params = {
          ...values,
          deliveryOrderDetails,
        }
        console.log(params)
        await dispatch(createOrderAsync(params))
        dispatch(getAllOrderAsync(pages))
        setIsModalOpen(false)
      }
    } else {
      values.orderDate = form.getFieldValue('orderDate').format('YYYY-MM-DD')
      const params = {
        ...values,
        deliveryOrderDetails,
      }
      await dispatch(editOrderAsync(params))
      dispatch(getAllOrderAsync(pages))
      console.log('dữ liệu update, ', params)
      setIsModalOpen(false)
    }
    //
    //
  }
  const handleDeleteOrder = async (id) => {
    await dispatch(deleteOrderAsync(id))
    // const updateSearchResult = order?.deleteOrderAsync?.items?.filter((item) => item.id !== id)
    // setSearchResult(updateSearchResult)
    dispatch(getAllOrderAsync(pages))
  }
  //modal => formProduct
  const [formProduct] = Form.useForm()

  const handleProduct = async (values) => {
    if (!values.id) {
      values.id = uuidv4()
      setDeliveryOrderDetails([...deliveryOrderDetails, { ...values }])
      console.log('product', values)
    } else {
      const updatedProductData = deliveryOrderDetails.map((item) =>
        item.id === values.id ? { ...item, ...values } : item
      )
      setDeliveryOrderDetails(updatedProductData)
    }
    // setDeliveryOrderDetails(values)
    formProduct.resetFields()
  }
  const EditProduct = (product) => {
    // console.log('pro', product)
    formProduct.resetFields()
    formProduct.setFieldsValue({ ...product })
  }
  const DeleteProduct = (id) => {
    // console.log(id)
    const updatedProductData = deliveryOrderDetails.filter(
      (item) => item.id !== id
    )
    setDeliveryOrderDetails(updatedProductData)
  }
  //search
  const dateFormat = 'DD/MM/YYYY'
  const [isAdvancedSearch, setIsAdvancedSearch] = useState(false)

  const [formSearch] = Form.useForm()
  const handleClear = () => {
    formSearch.resetFields()
    // setSearchResult("")
    formSearch.resetFields('')
  }
  const handleSearch = async (values) => {
    if (isAdvancedSearch) {
      const params = {
        ...values,
        codeDateFrom: dayjs(values?.createdDate?.[0]).format('YYYY-MM-DD'),
        CodeDateTo: dayjs(values?.createdDate?.[1]).format('YYYY-MM-DD'),
      }
      console.log('nâng cao', params)
      delete params.createdDate
      await dispatch(getAllOrderAsync({ ...params, ...pages }))
    } else {
      const params = {
        ...values,
      }
      console.log('bình thường', params)
      await dispatch(getAllOrderAsync({ ...params, ...pages }))
    }

    // delete params.createdDate
    // await dispatch(getAllOrderAsync({ ...params, ...pages }))
  }
  const advanceResult = (e) => {
    setIsAdvancedSearch(e.target.checked)
  }
  // //edit
  const handleEdit = async (id) => {
    setIsModalOpen(true)
    await dispatch(getOneOrderAsync(id))
    console.log('dữ liêu edit', order?.order)
    form.resetFields()
    formProduct.resetFields()
    setDeliveryOrderDetails('')
    form.setFieldsValue({
      saleStaff: order?.order?.saleStaff,
      id: order?.order?.id,
      shipper: order?.order?.shipper,
      consignee: order?.order?.consignee,
      fromAddress: order?.order?.fromAddress,
      toAddress: order?.order?.toAddress,
      totalAmount: order?.order?.totalAmount,
      sendType: order?.order?.sendType,
      receiveType: order?.order?.receiveType,
      provinceCode: order?.order?.provinceCode,
      paymentType: order?.order?.paymentType,
      orderDate: dayjs(order?.order?.orderDate),
      isDone: order?.order?.isDone,
      consigneePhone: order?.order?.consigneePhone,
      shipperPhone: order?.order?.shipperPhone,
      additionalAmount: order?.order?.additionalAmount,
    })
    setDeliveryOrderDetails(order?.order?.deliveryOrderDetails)
  }
  //modal bill of account
  const [isOpenModal, setIsOpenModal] = useState(false)
  const handleOkModal = () => {
    setIsOpenModal(false)
  }
  const handleCancelModal = () => {
    setIsOpenModal(false)
  }
  const showModalOfBill = async (id) => {
    setIsOpenModal(true)
    console.log(id)
    await dispatch(getOneOrderOfCodeAsync(id))
    // console.log(order)
  }
  return (
    <div>
      <Row>
        <Form
          labelCol={{
            span: 24,
          }}
          wrapperCol={{
            span: 24,
          }}
          form={formSearch}
          name='formSearch'
          onFinish={handleSearch}
          autoComplete='off'
          className='formSearch'
        >
          <Row>
            <Col span={6}>
              <Form.Item colon={true} label='Mã code' name='Code'>
                <Input
                  className='input-item'
                  // value={inputSearch}
                  // onChange={handleChangeCode}
                />
              </Form.Item>
            </Col>

            <Col span={6}>
              <Form.Item label='Trạng thái đơn hàng' name='Status'>
                <Select
                  options={[
                    {
                      value: 'New',
                      label: 'Đơn mới',
                    },
                    {
                      value: 'Inventory',
                      label: 'Đơn hàng tồn kho',
                    },
                    {
                      value: 'Gone',
                      label: 'Đơn hàng đã đi',
                    },
                    {
                      value: 'Transhipment',
                      label: 'Đơn hàng có phát sinh',
                    },
                  ]}
                  defaultValue={{
                    label: 'Chọn trạng thái đơn hàng',
                  }}
                  // onSelect={handleChangeStatus}
                />
              </Form.Item>
            </Col>

            <Col span={6}>
              <Form.Item
                colon={false}
                label='Hình thức thanh toán'
                name='PaymentType'
              >
                <Select
                  options={[
                    {
                      value: 'TTS',
                      label: 'TTS',
                    },
                    {
                      value: 'TDN',
                      label: 'TĐN',
                    },
                    {
                      value: 'DTT',
                      label: 'ĐTT',
                    },
                    {
                      value: 'Other',
                      label: 'Khác',
                    },
                  ]}
                  defaultValue={{
                    label: 'Chọn hình thức thanh toán',
                  }}
                  // onSelect={handleChangePayMent}
                />
              </Form.Item>
            </Col>

            <Col span={6}>
              <Form.Item colon={false} label='Đơn hàng hoàn tất' name='isDone'>
                <Select
                  options={[
                    {
                      value: 'true',
                      label: 'Đơn Hàng đã hoàn tất',
                    },
                    {
                      value: 'false',
                      label: 'Đơn Hàng chưa hoàn tất',
                    },
                  ]}
                  defaultValue={{
                    label: 'Chọn đơn hàng đã hoàn tất hay chưa',
                  }}
                  // onSelect={handleChangeDone}
                />
              </Form.Item>
            </Col>

            {isAdvancedSearch && (
              <Row>
                <Col span={6}>
                  <Form.Item
                    colon={true}
                    label='Nhân viên kinh doanh'
                    name='SaleStaff'
                  >
                    <Select
                      defaultValue={{
                        label: 'Chọn trạng nhân viên kinh doanh',
                      }}
                    >
                      {order?.staff?.map((staff) => (
                        <Option key={staff?.userName} value={staff?.userName}>
                          {staff?.fullName}
                        </Option>
                      ))}
                    </Select>
                  </Form.Item>
                </Col>

                <Col span={6}>
                  <Form.Item
                    label='Từ ngày - đến ngày'
                    name='createdDate'
                    initialValue={[dayjs('01/01/2022', dateFormat), dayjs()]}
                  >
                    <RangePicker format={dateFormat} />
                  </Form.Item>
                </Col>

                <Col span={6}>
                  <Form.Item
                    colon={true}
                    label='Địa điểm nhận hàng'
                    name='fromAddress'
                  >
                    <Input />
                  </Form.Item>
                </Col>

                <Col span={6}>
                  <Form.Item
                    colon={true}
                    label='Địa điểm giao hàng'
                    name='toAddress'
                  >
                    <Input />
                  </Form.Item>
                </Col>

                <Col span={6}>
                  <Form.Item colon={true} label='Tên khách hàng' name='shipper'>
                    <Input />
                  </Form.Item>
                </Col>
              </Row>
            )}
          </Row>
        </Form>
        <Row>
          <Col span={24} style={{ padding: '0' }}>
            <Space>
              <button className='btn-order' onClick={handleClear}>
                Clear
              </button>
              <button className='btn-order' form='formSearch'>
                Tìm kiếm
              </button>
              <button className='btn-order'>Export Excel</button>
              <button className='btn-order' onClick={showModal}>
                Tạo mới đơn
              </button>
              <Checkbox onChange={advanceResult}>Tìm kiếm nâng cao</Checkbox>
              <Radio.Group
                onChange={handleRadioChange}
                value={isExpanded ? 'expand' : 'collapse'}
              >
                <Radio value='collapse'>Rút gọn</Radio>
                <Radio value='expand'>Toàn bộ cột</Radio>
              </Radio.Group>
            </Space>
          </Col>
        </Row>
      </Row>

      <Row className='mt-5'>
        <Table
          style={{ width: '100%' }}
          dataSource={order?.deliveryOrder?.items}
          loading={order?.isLoading}
          columns={isColumns}
          pagination={{
            size: 'small',
            total: order?.deliveryOrder?.total,
            showTotal: (total, range) =>
              `${range[0]}-${range[1]} of ${total} items`,
          }}
          onChange={(page) => handleTableChange(page)}
        />
      </Row>
      <Row>
        <Modal
          title='Tạo mới đơn'
          open={isModalOpen}
          onOk={handleOk}
          onCancel={handleCancel}
          width={720}
          footer={[
            <button key='cancel' onClick={handleCancel} className='btn-order'>
              Cancel
            </button>,
            <button
              key='ok'
              htmlType='submit'
              type='primary'
              form='form'
              className='btn-order'
            >
              OK
            </button>,
          ]}
        >
          <Form
            onFinish={onFinish}
            autoComplete='off'
            form={form}
            name='form'
            labelCol={{
              span: 24,
            }}
            wrapperCol={{
              span: 24,
            }}
          >
            <Row>
              <Col span={12}>
                <Form.Item colon={true} label='Tạo mã vận đơn' name='isGenCode'>
                  <Radio.Group
                    defaultValue={isGenCode}
                    value={false}
                    onChange={(e) => setIsGenCode(e.target.value)}
                  >
                    <Radio value={true}>Tạo</Radio>
                    <Radio value={false}>Không tạo</Radio>
                  </Radio.Group>
                </Form.Item>
              </Col>

              <Col span={12}>
                <Form.Item label='Nhân viên kinh doanh' name='saleStaff'>
                  <Select
                    defaultValue={{
                      label: user?.fullName,
                      value: user?.userName,
                    }}
                  >
                    {order.staff.map((item, index) => (
                      <Select.Option
                        key={index}
                        value={item.userName}
                        onChange={(e) => setSaleStaff(e.target.value)}
                      >
                        {item.fullName}
                      </Select.Option>
                    ))}
                  </Select>
                </Form.Item>
              </Col>

              <Col span={12}>
                <Form.Item colon={true} label='Số mã' name='id'>
                  <Input disabled />
                </Form.Item>
              </Col>

              <Col span={12}>
                <Form.Item
                  colon={true}
                  label='Ngày tạo'
                  name='orderDate'
                  initialValue={dayjs()}
                >
                  <DatePicker
                    format={dateFormat}
                    defaultValue={dayjs().format(dateFormat)}
                    style={{
                      width: '100%',
                    }}
                  />
                </Form.Item>
              </Col>

              <Col span={12}>
                <Form.Item
                  colon={true}
                  label='Người gửi'
                  name='shipper'
                  rules={[
                    {
                      required: true,
                      message: 'Vui lòng nhập người gửi!',
                    },
                  ]}
                >
                  <Input />
                </Form.Item>
              </Col>

              <Col span={12}>
                <Form.Item
                  colon={true}
                  label='Người nhận'
                  name='consignee'
                  rules={[
                    {
                      required: true,
                      message: 'Vui lòng nhập người nhận!',
                    },
                  ]}
                >
                  <Input />
                </Form.Item>
              </Col>

              <Col span={12}>
                <Form.Item
                  colon={true}
                  label='Địa chỉ gửi'
                  name='fromAddress'
                  rules={[
                    {
                      required: true,
                      message: 'Vui lòng nhập địa chỉ gửI!',
                    },
                  ]}
                >
                  <Input />
                </Form.Item>
              </Col>

              <Col span={6}>
                <Form.Item
                  colon={true}
                  label='Địa chỉ nhận'
                  name='toAddress'
                  rules={[
                    {
                      required: true,
                      message: 'Vui lòng nhập địa chỉ nhận!',
                    },
                  ]}
                >
                  <Input />
                </Form.Item>
              </Col>

              <Col span={6}>
                <Form.Item
                  colon={true}
                  label='Tỉnh'
                  name='provinceCode'
                  rules={[
                    {
                      required: true,
                      message: 'Vui lòng nhập tỉnh!',
                    },
                  ]}
                >
                  <Select>
                    {PROVINCE?.map((province) => (
                      <Option key={province.code} value={province.code}>
                        {province.name}
                      </Option>
                    ))}
                  </Select>
                </Form.Item>
              </Col>

              <Col span={12}>
                <Form.Item
                  colon={true}
                  label='Số điện thoại người gửi'
                  name='shipperPhone'
                  rules={[
                    {
                      required: true,
                      message: 'Vui lòng nhập SĐT người gửi!',
                    },
                  ]}
                >
                  <Input />
                </Form.Item>
              </Col>

              <Col span={12}>
                <Form.Item
                  colon={true}
                  label='Số điện thoại người nhận'
                  name='consigneePhone'
                  rules={[
                    {
                      required: true,
                      message: 'Vui lòng nhập SĐT người nhận!',
                    },
                  ]}
                >
                  <Input />
                </Form.Item>
              </Col>

              <Col span={12}>
                <Form.Item
                  colon={true}
                  label='Hình thức nhận hàng'
                  name='receiveType'
                  rules={[
                    {
                      required: true,
                      message: 'Vui lòng nhập hình thức nhận hàng!',
                    },
                  ]}
                >
                  <Select
                    options={[
                      {
                        value: 'TN',
                        label: 'Tận nơi',
                      },
                      {
                        value: 'TK',
                        label: 'Tại kho',
                      },
                    ]}
                    defaultValue={{
                      label: 'Chọn hình thức nhận hàng',
                    }}
                  />
                </Form.Item>
              </Col>

              <Col span={12}>
                <Form.Item
                  colon={true}
                  label='Hình thức giao hàng'
                  name='sendType'
                  rules={[
                    {
                      required: true,
                      message: 'Vui lòng nhập hình thức giao hàng!',
                    },
                  ]}
                >
                  <Select
                    options={[
                      {
                        value: 'TN',
                        label: 'Tận nơi',
                      },
                      {
                        value: 'TK',
                        label: 'Tại kho',
                      },
                      {
                        value: 'QL',
                        label: 'Quốc lộ',
                      },
                    ]}
                    defaultValue={{
                      label: 'Chọn hình thức giao hàng',
                    }}
                  />
                </Form.Item>
              </Col>

              <Col span={12}>
                <Form.Item
                  colon={true}
                  label='Hình thức thanh toán'
                  name='paymentType'
                  rules={[
                    {
                      required: true,
                      message: 'Vui lòng nhập hình thức thanh toán!',
                    },
                  ]}
                >
                  <Select
                    options={[
                      {
                        value: 'TTS',
                        label: 'TTS',
                      },
                      {
                        value: 'TDN',
                        label: 'TĐN',
                      },
                      {
                        value: 'DTT',
                        label: 'ĐTT',
                      },
                      {
                        value: 'Other',
                        label: 'Khác',
                      },
                    ]}
                    defaultValue={{
                      label: 'Chọn hình thức thanh toán',
                    }}
                  />
                </Form.Item>
              </Col>

              <Col span={12}>
                <Form.Item
                  colon={true}
                  label='Cước vận chuyển'
                  name='totalAmount'
                  rules={[
                    {
                      required: true,
                      message: 'Vui lòng nhập cước vận chuyển!',
                    },
                  ]}
                >
                  <InputNumber
                    style={{
                      width: '100%',
                    }}
                    formatter={(value) =>
                      `${value}`.replace(/\B(?=(\d{3})+(?!\d))/g, ',')
                    }
                    parser={(value) => value.replace(/\$\s?|(,*)/g, '')}
                  />
                </Form.Item>
              </Col>

              <Col span={12}>
                <Form.Item
                  colon={true}
                  label='Phat sinh khác'
                  name='additionalAmount'
                >
                  <InputNumber
                    style={{
                      width: '100%',
                    }}
                    formatter={(value) =>
                      `${value}`.replace(/\B(?=(\d{3})+(?!\d))/g, ',')
                    }
                    parser={(value) => value.replace(/\$\s?|(,*)/g, '')}
                  />
                </Form.Item>
              </Col>
            </Row>
          </Form>

          <Col span={24}>
            <Card title='Tao/Sua đơn hàng' bordered={true}>
              <Form
                labelCol={{
                  span: 24,
                }}
                wrapperCol={{
                  span: 24,
                }}
                form={formProduct}
                name='formProduct'
                onFinish={handleProduct}
                autoComplete='off'
              >
                <Row>
                  <Col span={0}>
                    <Form.Item hidden label='id' name='id'>
                      <Input disabled />
                    </Form.Item>
                  </Col>

                  <Col span={12}>
                    <Form.Item
                      colon={true}
                      label='Tên hàng'
                      name='name'
                      rules={[
                        {
                          required: true,
                          message: 'Vui lòng nhập tên hàng!',
                        },
                      ]}
                    >
                      <Input />
                    </Form.Item>
                  </Col>

                  <Col span={12}>
                    <Form.Item
                      colon={true}
                      label='Số lượng'
                      name='quantity'
                      rules={[
                        {
                          required: true,
                          message: 'Vui lòng nhập số lượng!',
                        },
                      ]}
                    >
                      <InputNumber
                        style={{
                          width: '100%',
                        }}
                      />
                    </Form.Item>
                  </Col>

                  <Col span={12}>
                    <Form.Item
                      colon={true}
                      label='Khối lượng'
                      name='mass'
                      rules={[
                        {
                          required: true,
                          message: 'Vui lòng nhập khối lượng!',
                        },
                      ]}
                    >
                      <InputNumber
                        style={{
                          width: '100%',
                        }}
                      />
                    </Form.Item>
                  </Col>

                  <Col span={12}>
                    <Form.Item
                      colon={true}
                      label='Trọng lượng'
                      rules={[
                        {
                          required: true,
                          message: 'Vui lòng nhập trọng lượng!',
                        },
                      ]}
                      name='weight'
                    >
                      <InputNumber
                        style={{
                          width: '100%',
                        }}
                      />
                    </Form.Item>
                  </Col>

                  <Col span={12}>
                    <Form.Item
                      colon={true}
                      label='Đơn vị tính'
                      rules={[
                        {
                          required: true,
                          message: 'Vui lòng nhập đơn vị tính!',
                        },
                      ]}
                      name='unit'
                    >
                      <Input />
                    </Form.Item>
                  </Col>

                  <Col span={12}>
                    <Form.Item
                      colon={true}
                      label='Ghi chú'
                      name='note'
                      rules={[
                        {
                          required: true,
                          message: 'Vui lòng nhập ghi chú!',
                        },
                      ]}
                    >
                      <Input />
                    </Form.Item>
                  </Col>
                </Row>
              </Form>
              <button type='submit' className='btn-order' form='formProduct'>
                Thêm
              </button>
            </Card>

            <Col className='mt-3'>
              {deliveryOrderDetails?.length > 0 && (
                <Table
                  columns={columModal}
                  dataSource={deliveryOrderDetails}
                  pagination={false}
                />
              )}
            </Col>
          </Col>
        </Modal>
      </Row>

      <Row>
        <Modal
          title='BIÊN NHẬN VẬN CHUYỂN'
          open={isOpenModal}
          width={1000}
          onOk={handleOkModal}
          onCancel={handleCancelModal}
          footer={[
            <button
              key='cancel'
              onClick={handleCancelModal}
              className='btn-order-cancel'
            >
              Đóng
            </button>,
            <button
              key='ok'
              htmlType='submit'
              type='primary'
              form='form'
              className='btn-order'
            >
              Tải xuống kế toán
            </button>,
            <button
              key='okla'
              htmlType='submit'
              type='primary'
              form='form'
              className='btn-order'
            >
              Tải xuống vận đơn
            </button>,
          ]}
        >
          <Row>
            <Col span={12} className='mb=3'>
              MVĐ: {order?.order?.code}
            </Col>
            <Col span={12}>
              <Row className='mb-3'>
                <Col span={12}>
                  Nhân Viên kinh doanh: {order?.order?.saleStaff}
                </Col>
                <Col span={12}>SĐT:</Col>
              </Row>
            </Col>
            <Col span={4}>Người gửi:</Col>
            <Col span={8}>{order?.order?.shipper}</Col>
            <Col span={4}>Người nhận</Col>
            <Col span={8}>{order?.order?.consignee}</Col>

            <Col span={4}>Địa chỉ gửi:</Col>
            <Col span={8}>{order?.order?.fromAddress}</Col>
            <Col span={4}>Địa chỉ nhận</Col>
            <Col span={8}>{order?.order?.toAddress}</Col>
            <Col span={4} className='mb-3'>
              Số điện thoại gửi:
            </Col>
            <Col span={8} className='mb-3'>
              {order?.order?.shipperPhone}
            </Col>
            <Col span={4} className='mb-3'>
              Số điện thoại nhận
            </Col>
            <Col span={8} className='mb-3'>
              {order?.order?.consigneePhone}
            </Col>
            <Col span={24}>Hai bên thống nhất lượng vận chuyển như sau</Col>
            <Col span={24}>
              <Table
                loading={order?.isLoading}
                className='mt-4 mb-4'
                dataSource={order?.order?.deliveryOrderDetails}
                columns={columnBill1}
                pagination={false}
              />
            </Col>
            <Col span={5}>Cước vận chuyển:</Col>
            <Col span={7}>{order?.order?.totalAmount?.toLocaleString()}</Col>
            <Col span={5}>Hình thức thanh toán:</Col>
            <Col span={7}>{order?.order?.paymentType}</Col>
            <Col span={5}>Hình thức nhận hàng:</Col>
            <Col span={7}>{order?.order?.receiveType}</Col>
            <Col span={5}>Hình thức giao hàng:</Col>
            <Col span={7}>{order?.order?.sendType}</Col>
            <Col span={24}>
              <h6 style={{ fontWeight: 'bold', marginTop: '1em' }}> Giá bán</h6>
            </Col>
            <Col span={24}>
              <table className='table-code'>
                <tr>
                  <th>Giá bán</th>
                  <th>Thành tiền</th>
                </tr>
                <tr>
                  <td>Bán ra</td>
                  <td>{order?.order?.totalAmount?.toLocaleString()}</td>
                </tr>
                <tr>
                  <td>Phát sinh khác</td>
                  <td>{order?.order?.additionalAmount}</td>
                </tr>
              </table>
            </Col>
            <Col span={24}>
              <h6 style={{ fontWeight: 'bold', marginTop: '1em' }}> Giá mua</h6>
            </Col>
          </Row>
        </Modal>
      </Row>
    </div>
  )
}

export default Order
