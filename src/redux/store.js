import { configureStore } from '@reduxjs/toolkit'
import orderSlice from '../pages/Order/orderSlice'
import driverSlice from '../pages/Driver/driverSlice'
import accountSlice from '../pages/CreateAccount/accountSlice'
import customerSlice from '../pages/Customer/customerSlice'
import billingSlice from '../pages/List/billingSlice'
export const store = configureStore({
  reducer: {
    billingSlice: billingSlice,
    orderSlice: orderSlice,
    driverSlice: driverSlice,
    customerData: customerSlice,
    accountSlice: accountSlice,
  },
})
