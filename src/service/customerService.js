import request from '../utils/request'
import queryString from 'query-string'

export const getCustomerService = (data) => {
  return request(
    `https://api-uat-anzen-tms.azurewebsites.net/api/Customers?${queryString.stringify(
      data
    )}`,
    {
      method: 'GET',
      data,
    }
  )
}

export const deleteCustomerService = (id) => {
  return request(
    `https://api-uat-anzen-tms.azurewebsites.net/api/Customers/${id}`,
    {
      method: 'DELETE',
    }
  )
}

export const editCustomerService = (data) => {
  return request('https://api-uat-anzen-tms.azurewebsites.net/api/Customers', {
    method: 'POST',
    data,
  })
}

export const createCustomerService = (data) => {
  return request(`https://api-uat-anzen-tms.azurewebsites.net/api/Customers`, {
    method: 'POST',
    data,
  })
}
