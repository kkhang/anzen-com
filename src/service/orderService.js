import request from "../utils/request"
import queryString from 'query-string'
export const getAllOrderService = (data) => {
    return request(`https://api-uat-anzen-tms.azurewebsites.net/api/DeliveryOrders?${queryString.stringify(data)}`, {
        method: 'GET'
    })
}
export const getOneOrderService = (id) => {
    return request(`https://api-uat-anzen-tms.azurewebsites.net/api/DeliveryOrders/${id}`, {
        method: 'GET'
    })
}
export const getSalseStaffService = () => {
    return request(`https://api-uat-anzen-tms.azurewebsites.net/api/DeliveryOrders/get-sale-staff`, {
        method: 'GET'
    })
}
export const createOrderService = (data) => {
    return request(`https://api-uat-anzen-tms.azurewebsites.net/api/DeliveryOrders`, {
        method: 'POST',
        data
    })
}
export const editOrderService = (data) => {
    return request(`https://api-uat-anzen-tms.azurewebsites.net/api/DeliveryOrders`, {
        method: 'POST',
        data
    })
}
export const deleteOrderService = (id) => {
    return request(`https://api-uat-anzen-tms.azurewebsites.net/api/DeliveryOrders/${id}`, {
        method: 'DELETE',
    })
}
export const getOrderOfCodeService = (id) => {
    return request(`https://api-uat-anzen-tms.azurewebsites.net/api/DeliveryOrders/${id}`, {
        method: 'GET',
    })
}
export const getDetailOfCodeService = (id) => {
    return request(`https://api-uat-anzen-tms.azurewebsites.net/api/DeliveryOrders/accounting/${id}`, {
        method: 'GET',
    })
}