import request from "../utils/request";
import queryString from "query-string";
export const getAllDriverService = (data) => {
  return request(
    `https://api-uat-anzen-tms.azurewebsites.net/api/Drivers?${queryString.stringify(
      data
    )}`,
    {
      method: "GET",
    }
  );
};
export const createDriverService = (data) => {
  return request(
    `https://api-uat-anzen-tms.azurewebsites.net/api/Drivers`,
    {
      method: "POST",
      data,
    }
  );
};
export const updateDriverService = (data) => {
  return request(
    `https://api-uat-anzen-tms.azurewebsites.net/api/Drivers`,
    {
      method: "POST",
      data,
    }
  );
};
export const deleteDriverService = (id) => {
  return request(
    `https://api-uat-anzen-tms.azurewebsites.net/api/Drivers/${id}`,
    {
      method: "DELETE",
    }
  );
};