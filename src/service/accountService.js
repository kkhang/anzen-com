import request from "../utils/request";
import queryString from "query-string";
export const getAllAccountService = (data) => {
  return request(
    `https://api-uat-anzen-tms.azurewebsites.net/api/ApplicationUser?${queryString.stringify(
      data
    )}`,
    {
      method: "GET",
    }
  );
};
export const getAllRoleService = (data) => {
  return request(
    `https://api-uat-anzen-tms.azurewebsites.net/api/roles?${queryString.stringify(
      data
    )}`,
    {
      method: "GET",
    }
  );
};
export const createAccountService = (data) => {
  return request(
    `https://api-uat-anzen-tms.azurewebsites.net/api/ApplicationUser`,
    {
      method: "POST",
      data,
    }
  );
};

export const updateIsActiveService = (data) => {
  return request(
    `https://api-uat-anzen-tms.azurewebsites.net/api/ApplicationUser/active`,
    {
      method: "POST",
      data,
    }
  );
};
export const updateRoleService = (data) => {
  return request(
    `https://api-uat-anzen-tms.azurewebsites.net/api/ApplicationUser/change-role`,
    {
      method: "POST",
      data,
    }
  );
};
