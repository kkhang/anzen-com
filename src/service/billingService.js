import request from "../utils/request"
import queryString from 'query-string'

export const getAllBillingService = (data) => {
    return request(`https://api-uat-anzen-tms.azurewebsites.net/api/BillOfLadings?${queryString.stringify(data)}`, {
        method: 'GET'
    })
}